const gulp = require('gulp'),
    less = require('gulp-less'),
    browserify = require('browserify'),
    template = require('gulp-template');


gulp.task('build', [
    'index',
    'images',
    'js',
    'css',
    'audio', 'fonts'
]);

gulp.task('less', () => {
    return gulp.src(['assets/less/app.less'])
        .pipe(less())
        .pipe(gulp.dest('dist/public'))
        .pipe(bs.reload({stream: true}))

})

gulp.task('css', ['less', 'styles-bower']);

gulp.task('js', ['browserify', 'js-lib', 'js-public']);

gulp.task('audio', () => {
    return gulp.src([
        'assets/audio/*.mp3'
    ]).pipe(gulp.dest('dist/public/audio'))
});


gulp.task('images', () => {
    return gulp.src([
        'assets/favicon/*'
    ]).pipe(gulp.dest('dist/public/images/favicon'))
});

gulp.task('js-public', () => {
    gulp.src([
        'bower_components/lodash/dist/lodash.js'
    ]).pipe(gulp.dest('dist/public/js/'))
});

gulp.task('js-lib', () => {
    gulp.src([
        'src/lib/console.js',
        'src/lib/auth.js',
        'src/lib/server.js',
    ]).pipe(gulp.dest('dist/'))
});

gulp.task('styles-bower', () => {
    gulp.src([
        'bower_components/font-awesome/css/font-awesome.css'
    ]).pipe(gulp.dest('dist/public/styles'))
});

gulp.task('fonts', () => {
    gulp.src([
        'bower_components/font-awesome/fonts/*'
    ]).pipe(gulp.dest('dist/public/fonts'))
});

gulp.task('index', () => {
    gulp.src([
        'src/index.html'
    ])
        .pipe(template({
            config:  {
                // todo get from config ??
                path: '/console'
            }
        }))
        .pipe(gulp.dest('dist/public'))
        .pipe(bs.reload({stream: true}))

});

gulp.task('dev', ['build', 'browser-sync'], () => {
    gulp.watch([
        'src/**/*.less'
    ], ['less']);

    gulp.watch([
        'src/**/*.vue',
        'src/**/*.js'
    ], ['js']);

    gulp.watch([
        'src/index.html',
    ], ['index']);
});


var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

gulp.task('browserify', function () {

    let b = browserify({
        entries: './src/main.js',
        debug: true,
        transform: [
            //require('vueify'),
            //require('babelify').configure({presets: ["es2015"]}),
            //require('aliasify'),
        ]
    })

    return b.bundle()
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(gulp.dest('./dist/public/'))
        .pipe(bs.reload({stream: true}))
});

var bs = require('browser-sync').create(); // create a browser sync instance.

gulp.task('browser-sync', function () {
    bs.init({
        /*server: {
            baseDir: "./"
        },*/
        proxy: {
            target: "localhost:4000/console",
            ws: true
        }
    });
});

gulp.task('default', ['build']);