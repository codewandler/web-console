module.exports = {
    staticUsers: function (users) {
        // todo hash passwords

        this.authenticated = query => {
            // get the user
            let u = users[query.user];
            if (!u) {
                return Promise.reject({
                    success: false,
                    message: `User does not exist`
                })
            }

            if (query.password !== u.password) {
                return Promise.reject({
                    success: false,
                    message: `Invalid Credentials`
                });
            }

            // all good
            return Promise.resolve({
                id: query.user,
                success: true
            })
        }

        return this;
    }
};