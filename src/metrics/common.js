import _ from 'lodash'

// todo this should come from the framework (smub) ...
const metrics = {
    DEFAULT: {
        unit: false,
        icon: 'table',
        type: 'number',
        precision: 0,
        unit: false,
    },
    response_time: {
        unit: 'ms',
        icon: 'clock-o'
    },
    os_load: {
        unit: '%',
        precision: 2,
        scale: 100,
        values: {
            max: {
                precision: 0,
                unit: '%',
                scale: 100
            }
        }
    },
    os_load_normalized: {
        unit: '%',
        precision: 2,
        scale: 100,
        values: {
            cores: {
                precision: 0,
                unit: '',
                scale: 1
            }
        }
    },
    os_uptime: {
        unit: 'seconds',
        precision: 0
    },
    os_memory: {
        unit: 'GByte',
        scale: 1.0 / (1024 * 1024 * 1024),
        precision: 2,
        values: {
            used_percent: {
                unit: '%',
                scale: 100,
                precision: 1
            },
            free_percent: {
                unit: '%',
                scale: 100,
                precision: 1
            },
            free_actual_percent: {
                unit: '%',
                scale: 100,
                precision: 1
            }
        }
    },
    random: {
        unit: 'unicorns / hr',
        precision: 2
    },
    'sys.disks.free': {
        values: {
            used_percent: {
                unit: '%'
            }
        },
        type: 'number',
        precision: 2,
        unit: 'gb',
        filter: m => {
            return m.values.used > 0
        }
    }
};

const iconMap = {
    '__default': 'table',
    'asterisk.activity': 'volume-control-phone',
    'room.temperature': 'bar-chart-o',
    'room.humidity': 'tint',
    'sys.disks.free': 'server',
    'task.done': 'tasks',
    'response_time': 'clock-o'
}

let get = id => {
    return _.merge({}, {
        unit: false,
        icon: iconMap[id],
        values: {},
        filter: () => {
            return true;
        }
    }, metrics[id] || {})
};

let getValue = (id, key) => {
    let i = get(id)
    if (key in i.values) {
        return i.values[key]
    }
    return i
}

let getN = function (id, key, n) {
    if (key) {
        return getValue(id, key)[n]
    } else {
        return get(id)[n]
    }
}

export default {
    get: get,

    getIcon: id => {
        return 'fa-' + get(id).icon
    },

    getUnit(id, key) {
        return getN(id, key, 'unit')
    },

    getType(id, key) {
        return getN(id, key, 'type')
    },

    getPrecision(id, key) {
        return getN(id, key, 'precision')
    },

    getScale(id, key) {
        return getN(id, key, 'scale')
    },

    iconMap: iconMap
}
