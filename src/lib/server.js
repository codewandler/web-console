// server-side dummy which starts a server and dispatches some dummy data

const Console = require('./console').Console;

let webConsole = new Console({
    port: 4000,
    users: {
        admin: {password: 'admin123'},
        devops: {password: 'devops'}
    }
});

webConsole.on('authenticated', auth => {
    console.log("authenticated", auth.auth)
});

webConsole.on('message', msg => {
    let {data, reply} = msg
    reply('thanks for your message !');
});

let emitMetric = () => {
    webConsole.broadcast({
        type: 'event',
        event: 'metric',
        data: {
            id: 'room.temperature',
            tags: {
                room: 'room4',
                host: 'localhost'
            },
            values: {
                temp_low: Math.random() * 100,
                temp_high: Math.random() * 100,
            }
        }
    })

    webConsole.broadcast({
        type: 'event',
        event: 'metric',
        data: {
            id: 'room.humidity',
            tags: {
                room: 'first_room',
                host: 'localhost'
            },
            values: {
                hum: Math.random() * 100,
            }
        }
    })
};

webConsole.on('listening', data => {
    console.log("web-console listening", data)
})

webConsole.start();

setInterval(() => {
    emitMetric();
}, 1000);
