const _ = require('lodash');
const EventEmitter = require('events').EventEmitter;

class ConsoleEmitter extends EventEmitter {
};

function Console(opts) {
    let self = this;

    let events = new ConsoleEmitter();

    let consoleOpts = _.merge({}, {
        port: 4000,
        host: '127.0.0.1',
        users: []
    }, opts);

    consoleOpts.path = '/console';

    let io, http;

    self.stop = () => {
        events.emit('stopping')

        // close the server
        http.close();

        // unbind listeners
        events.removeAllListeners();

        events.emit('stop')
    };

    self.start = () => {
        // determine the correct web-root
        const webRoot = require('path').resolve(__dirname, './public/');

        const express = require('express');
        var app = express();
        http = require('http').Server(app);

        // bind path to static folder
        app.use(consoleOpts.path, express.static(webRoot));

        // start server
        http.listen(consoleOpts.port, consoleOpts.host, function () {

            events.emit('listening', {
                port: consoleOpts.port,
                webRoot: webRoot,
                message: `listening on *:${consoleOpts.port}`
            });
        });

        io = require('socket.io')(http, {
            path: `${consoleOpts.path}/socket.io`
        });

        let auth = require('./auth').staticUsers(consoleOpts.users);

        io.on('connection', (socket) => {
            let query = socket.handshake.query;
            let user;

            auth.authenticated(query).then(auth => {
                user = auth;

                socket.on('message', data => {
                    events.emit('message', {
                        user: auth,
                        data: data,
                        reply: (data) => {
                            socket.send(data)
                        }
                    });
                })

                socket.send('authenticated');

                events.emit('authenticated', {
                    socket: socket,
                    auth: auth
                })

            }, err => {
                events.emit('auth_failed', {
                    message: err.message,
                    query: query
                });

                socket.send('unauthorized');

                // close the socket
                socket.disconnect('unauthorized');
            });

            socket.on('disconnected', () => {
                events.emit('disconnected', user)
                // todo further cleanup
            });

            socket.on('close', () => {
                events.emit('close', user)
                // todo further cleanup
            });
        });
    };

    return {
        start: () => {
            return self.start()
        },
        on: (name, cb) => {
            events.on(name, cb)
        },
        broadcast: (data) => {
            io.sockets.emit('message', data);
        },
        stop: () => {
            return self.stop();
        }
    }
}

module.exports = {
    Console: Console
};