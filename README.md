# smub - web-console

**Install**

```bash
# install dependencies
npm install

# build
gulp
```

**Start the Demo Console**

```bash
# start the demo server
node src/lib/index.js

# ... or
npm start
```

Then visit: `http://localhost:4000`

-------------

# todo
- implement security via username & password
- individual renderer per metric id
- write some tests

## build
- minify app.js & css

## UI
- create the task view - backend has to remain state over them
- filters: by host, by target, by query - should affect metric- and task view 
- change favicon to red-dot when a current alert is present
